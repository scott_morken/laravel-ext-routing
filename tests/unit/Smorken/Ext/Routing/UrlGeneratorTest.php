<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/16/16
 * Time: 10:18 AM
 */
use Mockery as m;

class UrlGeneratorTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testActionNoRouteThrowsException()
    {
        list($sut, $routes, $request) = $this->getSutAndMocks();
        $routes->shouldReceive('getByAction')->once()->with('\App\Foo\Controller@action')->andReturnNull();
        $routes->shouldReceive('getByAction')->once()->with('App\Foo\Controller@action')->andReturnNull();
        $sut->action('\App\Foo\Controller@action');
    }

    protected function getSutAndMocks()
    {
        $routes = m::mock(Illuminate\Routing\RouteCollection::class);
        $request = m::mock(Illuminate\Http\Request::class);
        $sut = new \Smorken\Ext\Routing\UrlGenerator($routes, $request);
        return [$sut, $routes, $request];
    }

    /**
     * @expectedException  \InvalidArgumentException
     */
    public function testActionNoRouteWithRootNamespaceThrowsException()
    {
        list($sut, $routes, $request) = $this->getSutAndMocks();
        $sut->setRootControllerNamespace('\Foo');
        $routes->shouldReceive('getByAction')->once()->with('\App\Foo\Controller@action')->andReturnNull();
        $routes->shouldReceive('getByAction')->once()->with('App\Foo\Controller@action')->andReturnNull();
        $routes->shouldReceive('getByAction')->once()->with('\Foo\App\Foo\Controller@action')->andReturnNull();
        $routes->shouldReceive('getByAction')->once()->with('Foo\App\Foo\Controller@action')->andReturnNull();
        $sut->action('\App\Foo\Controller@action');
    }

    public function testActionWithRouteReturns()
    {
        list($sut, $routes, $request) = $this->getSutAndMocks();
        $route = m::mock('Illuminate\Contracts\Routing\Route');
        $routes->shouldReceive('getByAction')->once()->with('\App\Foo\Controller@action')->andReturn($route);
        $route->shouldReceive('domain')->once()->andReturn(true);
        $route->shouldReceive('httpOnly')->once()->andReturn(true);
        $request->shouldReceive('isSecure')->once()->andReturn(true);
        $request->shouldReceive('getPort')->once()->andReturn(443);
        $route->shouldReceive('uri')->once()->andReturn('foo/action');
        $r = $sut->action('\App\Foo\Controller@action');
        $this->assertEquals('http://1/foo/action', $r);
    }
}
