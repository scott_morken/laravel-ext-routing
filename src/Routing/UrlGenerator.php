<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/6/15
 * Time: 9:56 AM
 */
namespace Smorken\Ext\Routing;

use InvalidArgumentException;

class UrlGenerator extends \Illuminate\Routing\UrlGenerator
{

    /**
     * Get the URL to a controller action.
     *
     * @param  string $action
     * @param  mixed $parameters
     * @param  bool $absolute
     * @return string
     * @throws \InvalidArgumentException
     */
    public function action($action, $parameters = [], $absolute = true)
    {
        $route = $this->findRouteByAction($action);
        if (!$route && $this->rootNamespace) {
            $action = rtrim($this->rootNamespace, "\\") . "\\" . ltrim($action, "\\");
            $route = $this->findRouteByAction($action);
        }
        if (is_null($route)) {
            throw new InvalidArgumentException("Action {$action} not defined.");
        }
        return $this->toRoute($route, $parameters, $absolute);
    }

    protected function findRouteByAction($action)
    {
        $r = $this->routes->getByAction($action);
        if (!$r) {
            $action = trim($action, "\\");
            $r = $this->routes->getByAction($action);
        }
        return $r;
    }

    /**
     * Get the Route URL generator instance.
     *
     * @return \Illuminate\Routing\RouteUrlGenerator
     */
    protected function routeUrl()
    {
        if (! $this->routeGenerator) {
            $this->routeGenerator = new RouteUrlGenerator($this, $this->request);
        }

        return $this->routeGenerator;
    }

    /**
     * Get the formatted domain for a given route.
     *
     * @param  \Illuminate\Routing\Route $route
     * @param  array $parameters
     * @return string
     *
     * * @throws \InvalidArgumentException
     */
    protected function getRouteDomain($route, &$parameters)
    {
        if (!$route) {
            throw new InvalidArgumentException('Route cannot be null');
        }
        return $route->domain() ? $this->formatDomain($route, $parameters) : null;
    }
}
