<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/16/17
 * Time: 11:01 AM
 */

namespace Smorken\Ext\Routing;

use InvalidArgumentException;

class RouteUrlGenerator extends \Illuminate\Routing\RouteUrlGenerator
{

    /**
     * Get the formatted domain for a given route.
     *
     * @param  \Illuminate\Routing\Route $route
     * @param  array $parameters
     * @return string
     */
    protected function getRouteDomain($route, &$parameters)
    {
        if (is_null($route)) {
            throw new InvalidArgumentException('Route cannot be null');
        }
        return $route->domain() ? $this->formatDomain($route, $parameters) : null;
    }
}
